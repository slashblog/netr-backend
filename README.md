# netr-backend

- This python script can be used to capture images from Hikvision NVR and upload images to a dropbox account (periodically).
- Dropbox is chosen because it offers 2.0 GB free data storage which is sufficient for my personal needs.
- Edit the script as per the instructions given in the code.
- The script can be suitably modified for other NVRs.
- Cleanup of old uploaded images is also supported. By default it keeps 3 days of historical data.

## Periodic execution of script

To upload the images and cleanup old images add the following cronjob entries:

To create cron job execute the command `cronjob -e`, and add the following lines (after making the necessary modifications): 
```
*/15 * * * * ${SCRIPT_PATH}/capture-pic-and-upload.py
0 2 * * * ${SCRIPT_PATH}/capture-pic-and-upload.py cleanup
```
With the above entries:
- Upload happens after every 15 minutes.
- Cleanup happens once everyday at 2 AM.
