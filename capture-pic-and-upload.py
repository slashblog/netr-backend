#!/usr/bin/python3

# Copyright 2022 author

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime, timedelta
import dropbox
import json
import os
import re
import requests
from requests.auth import HTTPDigestAuth
import sys

# This script is designed for Hikvision NVR. For other NVRs this needs to modified.

# Dropbox SDK docs: https://dropbox-sdk-python.readthedocs.io/en/master/moduledoc.html

##########################################################################################
# Edit the following lines with right values #############################################
# START OF CONFIGURATION DATA ############################################################
##########################################################################################

# Dropbox access token :- generate a permanent one
DB_ACCESS_TOKEN = '__DB_ACCESS_TOKEN__'
# Path where you want to upload images. E.g. /location
LOCATION = '/location'
# Camera/NVR host name or IP address
HOST = '__HOST__'
# Camera/NVR user name
USERNAME = '__USER__'
# Camera/NVR user password
PASSWORD = '__PASSWORD__'
# The number of days for which to keep historical images
HISTORY_TO_KEEP = 3
# High resolution
HIGH = {
    'X': 1920,
    'Y': 1080
}
# Low resolution
LOW = {
    'X': 640,
    'Y': 360
}
# List of camera. It is a python dict with key being camera-name and value being offset as
# used in URL below. Add as many entries as there are cameras. Note camera name is human
# readable text, for your own identification. It should preferably have same name as what
# you configured in your NVR.
CAMS = {
    'camera-1': '101',
    'camera-2': '201',
    'camera-3': '601',
    'camera-4': '401',
    'camera-5': '301',
    'camera-6': '501',
    'camera-7': '701',
}
##########################################################################################
# END OF CONFIGURATION DATA ##############################################################
##########################################################################################

NOW = datetime.now()
FILE = NOW.strftime('%Y-%m-%d-%H:%M')
DEST = LOCATION + NOW.strftime('/%Y-%m-%d')
URL = 'http://%s/ISAPI/Streaming/channels/%s/picture?videoResolutionWidth=%d&videoResolutionHeight=%d'

dbx = dropbox.Dropbox(DB_ACCESS_TOKEN)

def exists(path):
    try:
        dbx.files_get_metadata(path)
        return True
    except:
        return False

def mkdir(directory):
    if not exists(directory):
        print('Creating directory in dropbox: ' + directory)
        dbx.files_create_folder(directory)

def download(camera, index, width, height):
    auth = HTTPDigestAuth(USERNAME, PASSWORD)
    url = URL % (HOST, index, width, height)
    requests.get(url, auth)
    print('Processing url: ' + url)
    result = bytearray()
    with requests.get(url, auth=auth, stream=True) as r:
        r.raise_for_status()
        for chunk in r.iter_content(chunk_size=8192): 
            result += bytearray(chunk)
    return result

def upload(cam_path, file_content):
    print('Saving camera grab to dropbox path: ' + cam_path)
    dbx.files_upload(bytes(file_content), cam_path, mode=dropbox.files.WriteMode('overwrite', None)) 

def find_files_to_be_deleted_recursively(path=None, do_not_delete=None):
    try:
        files_to_be_deleted = []

        if path is None:
            res = dbx.files_list_folder(LOCATION, recursive=True)
            do_not_delete = '^' + LOCATION + '($|/(latest.json|'
            skips = []
            for i in range(0, HISTORY_TO_KEEP):
                skips.append((NOW - timedelta(i)).strftime('%Y-%m-%d'))
            do_not_delete += '|'.join(skips) + '))'
            print('Following paths will be preserved: ' + do_not_delete)
            do_not_delete = re.compile(do_not_delete)
        else:
            res = dbx.files_list_folder_continue(path)
        for entry in res.entries:
            if not do_not_delete.match(entry.path_display):
                files_to_be_deleted.append(entry.path_display)
        if res.has_more:
            files_to_be_deleted.extend(find_files_to_be_deleted_recursively(res.cursor, do_not_delete))

        return files_to_be_deleted
    except dropbox.exceptions.ApiError as ex:
        print('Error: ' + str(ex))
   
if len(sys.argv) > 1 and sys.argv[1] == 'cleanup':
    files_to_be_deleted = find_files_to_be_deleted_recursively()
    if len(files_to_be_deleted) == 0:
        print('No files need to be deleted')
    else:
        entries = list(map(lambda item: dropbox.files.DeleteArg(item), files_to_be_deleted))
        dbx.files_delete_batch(entries)
        print('Delete request has been dispatched')
else:
    mkdir(LOCATION)
    mkdir(DEST)
    i = 1
    for camera, index in CAMS.items():
        cam_path = DEST + '/' + FILE + '-%s-%d-' + camera + '.jpg'
    
        upload(cam_path % ('high', i), download(camera, index, HIGH['X'], HIGH['Y']))
        upload(cam_path % ('low', i), download(camera, index, LOW['X'], LOW['Y']))
        
        i = i + 1
    
    last_update = json.dumps({
        'dest': DEST,
        'time': FILE,
        'cameras': list(CAMS.keys()),
        'types': ['high', 'low'],
        'thumb_type': 'low',
        'image_type': 'high'
    })
    
    upload(LOCATION + '/latest.json', last_update.encode())


